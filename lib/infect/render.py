""" A simple module to plot infection model results """
import matplotlib.pyplot
import mpld3
import numpy
import os


def plot(scenario, filename=None, display=False):
    df = scenario.dataframe
    timeframe = numpy.linspace(0, len(df)-1, len(df)) 
    fig, ax = matplotlib.pyplot.subplots(1,1,figsize=(11,8.5))
    for col in df.columns:
        ax.plot(timeframe, df[col], alpha=0.7, linewidth=3, label=col)

    ax.set_xlabel('Time (days)')

    ax.yaxis.set_tick_params(length=0)
    ax.xaxis.set_tick_params(length=0)
    ax.grid(b=True, which='major', c='w', lw=2, ls='-')
    legend = ax.legend(borderpad=2.0)
    legend.get_frame().set_alpha(0.5)

    for spine in ('top', 'right', 'bottom', 'left'):
        ax.spines[spine].set_visible(False)

    # Top/total line
    # ax.plot(t, S+I+R, 'c--', alpha=0.7, linewidth=2, label='Total')

    if display:
        matplotlib.pyplot.show()
    if filename:
        matplotlib.pyplot.savefig(os.path.expanduser(filename), dpi=300)

def export_history(scenario, filename):
    wb = xlsxwriter.Workbook(filename)
    
    # add the data tab
    data = wb.add_worksheet('Data')
    data.write_row(0,0, scenario.columns)        
    for i,row in enumerate(scenario.history):            
        data.write_row(i+1, 0, row)
    
    # add the infection chart
    infection = wb.add_chartsheet('Infections')
    graph = wb.add_chart({'type': 'line'})
    graph.set_size({'width': 1600, 'height': 1200})
    for i in range(len(scenario.columns)):
        column = chr(ord('A') + i)            
        graph.add_series({
            'name': f'Data!${column}$1',
            'values': f'Data!${column}$2:${column}${self.day}'
        })
    infect.set_chart(graph)
    wb.close()
