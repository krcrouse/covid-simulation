
class Infection():
    """ A class for parameters related to the infection. At present, everything is a class-level variable, but that doesn't prevent future use creating different strains with different parameters or adding in other infections (such as the flu).
        
        Disease/Contagiousness Attributes:
            incubation (int): The number of days following infection before the person is contagious.
            contagious_days (int): The number of days that the individual is contagious for following incubation.
            transmissability (float): The chance that any given 'meaningful interaction' on any given day may result in infection.

        Complication Attributes: These are the rates that individuals infected with the infection need advanced care. At present, this is evaluated on the last day of contagion but may need to be altered in future revisions:
            hospitalization (float): the rate of admission to the hospital
            icu (float): the rate of admission to the ICU
            deceased (float): the percent of infections that are deceased.
    """
    incubation = None
    contagious_days = None
    transmissability = None
    complications = None

class SimpleCOVID(Infection):
    """ This is intended to compare to the SIR models """
    contagious_days = 10


class COVID(Infection):
    """ This is intended to be a standard model based on current estimates. """
 
    incubation = 5
    contagious_days = 10

    complications = ['deceased', 'hospitalization', 'icu']
    deceased = 0.0100
    hospitalization = 0.025
    icu = 0.0075
    
