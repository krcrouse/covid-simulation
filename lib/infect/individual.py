import random

class Individual():
    daily_contacts = 16
    protection_rate = 0
    protection_frequency = 1

    def __init__(self, scenario, infected=False):
        self.scenario = scenario
        self.infected = False
        self.recovered = False
        self.contagious = False
        self.incubating = False
        self.deceased = False
        self.days_contagious = None
        self.incubation_remaining = None
        if self.scenario.infection.complications:
            for complication in self.scenario.infection.complications:
                setattr(self, complication, False)

        if infected:
            self.infect()


    def contacts_during_infection(self):
        """This is adapated from c in the SIR model - the total number of contacts across the contagious period of the infection."""
        return(self.scenario.infection.contagious_days * self.daily_contacts)

    @property
    def never_infected(self):
        return(not self.ever_infected)

    @property
    def ever_infected(self):
        return(self.recovered or self.infected or self.deceased)

    def infect(self):
        if self.ever_infected:
            return(False)
        self.infected = True
        self.date_infected = self.scenario.day
        if self.scenario.infection.incubation:
            self.incubating = True
            self.incubation_remaining = self.scenario.infection.incubation
        else:
            self.contagious = True
            self.incubating = False
            self.days_contagious = self.scenario.infection.contagious_days
        return(True)

    def nextday(self):
        if self.infected:
            if self.days_contagious == 0:
                # time to transition out of infection!
                # we have reached the end of the infection.  
                self.contagious = False
                self.infected = False
                # Evaluate any complications as a result of the infection
                if self.scenario.infection.complications:
                    for complication in self.scenario.infection.complications:
                        complication_rate = getattr(self.scenario.infection, complication)
                        r = random.random()
                        setattr(self, complication, r <= complication_rate)
                # determine if this case was deceased
                if not self.deceased:
                    self.recovered = True                      
            elif self.days_contagious:                          
                self.days_contagious -= 1
            elif self.incubation_remaining == 0:
                # time to transition between incubation and contagion!
                self.contagious = True
                self.incubating = False
                self.days_contagious = self.scenario.infection.contagious_days
            elif self.incubation_remaining:
                self.incubation_remaining -= 1
            else:
                raise Exception("Bad state - infected but neither contagious nor incubating?")


    def evaluate_contact(self, agent):
        """ """
        if self.contagious:
            chance = self.scenario.infection.transmissability * (1 - self.protection_rate*self.protection_frequency) * (1 - agent.protection_rate*agent.protection_frequency)
            r = random.random()
            #print(f"Random {r} vs {chance}")
            return(r <= chance)

    def contact(self, agent):
        """ Indicates contact has occurred between two individuals - determine if infection happens. We use the formula:
            
            chance of infection = my.transmissability * (1 - my.protection_rate) * (1 - other.protection_rate) """
        if self.contagious:
            chance = self.scenario.infection.transmissability * (1 - self.protection_rate*self.protection_frequency) * (1 - self.protection_rate*self.protection_frequency)
            r = random.random()
            #print(f"Random {r} vs {chance}")
            if r <= chance:                
                agent.infect()
    