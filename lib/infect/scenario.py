import datetime
import infect
import math
import pandas
import os
import csv

''' 
The Scenario is the context entity for the simulations. End users should generally instantiate a Scenario first and then build out the model from there. The Scenario comes with defaults to get going as quickly as possible.
'''


class Scenario():

    columns = ['never_infected', 'ever_infected', 'recovered', 'infected', 'incubating', 'contagious', 'deceased']


    def __init__(self, infection=None, days=250, start_date=None):
        self.days = days

        if start_date:
            self.start_date = start_date
        else:
            self.start_date = datetime.datetime.today()

        if not infection:
            infection = infect.infection.COVID()
        self.infection = infection

        self.day = -1
        self.population = []
        self.history = []

        self.columns = self.columns.copy()
        if self.infection.complications:
            self.columns += self.infection.complications


    def copy_context(self):
        "Copies the current scenario, so you can run different branching models."
        import copy
        new_context =copy.deepcopy(self)
        return(new_context)

    def set_doubling_rate(self, days_to_double):
        """ Uses the theoretrical doubling rate from the SIR model to set the transmissability. This method should only be used during start up to define the transmissability for a static population. It will use the number of contacts per day from the first individual and the number of contagious days from the infection to determine the transmissability."""

        # the growth rate when the entire population is suceptiable (e.g. at t=0) is 2^(1/Td) - 1
        growth_0 = 2**(1/days_to_double)-1
        # Beta / Infectivity is equal to (growth_0 + gamma)
        beta = growth_0 + 1/self.infection.contagious_days
        self.infection.transmissability = beta / self.population[0].daily_contacts 

    def doubling_rate(self):
        """ gets the doubling rate (in days) based on current parameters for the infection and the first person of the population."""

        time_to_double = 1 / math.log((self.infectivity - 1/self.infection.contagious_days + 1), 2)
        return(time_to_double)

    @property
    def infectivity(self):
        """ This is the Beta of the SIR model, defined by transmissibility [tau] * total number of contacts per day [c]. 
        
        Returns:
            float: The beta term in the SIR model, assuming the first individual in the population is representative.
        Note: 
            In some representations of the SIR model, the beta term incorporates a population parameters, e.g. tau * c / N. However, because pyinfect allows for both population-based engines (like SIR) and individual-based engines, the parameter here is agnostic to the population size. As noted by Sextus Empiricus, the distinction here is whether beta represents 'infections per infected per susceptible' or 'infections per infected.' In this representation, beta is the "infections per infected." 
        Acknowledgements:
            Thank you to [Sextus Empiricus](https://stats.stackexchange.com/users/164061/sextus-empiricus) for helping me to understand the beta and transmissibility parameters, especially in regard to the population parameter.

 
            """ 
        beta = self.population[0].daily_contacts * self.infection.transmissability
        return(beta)

    beta = infectivity

    @property
    def gamma(self):
        """float: This is the gamma of the SIR model, or 1 / (mean_days_contagious) """
        return(1 / self.infection.contagious_days)

    @property
    def reproduction_number(self):
        """ r0 - the basic reproduction number. In SIR terms, this is R_t = Beta / rho, also (infections per contact) * (contacts during infection) * (time of contagion). However, because the agent model considers infections per agent:agent contact and SIR considers a global per infection rate, we need to translate our parameters to the total reproduction over the entire period of contagion """
        return(self.infectivity * self.infection.contagious_days)


    @property
    def count(self):
        return(len(self.population))

    def add_population(self, count, infected=False):        
        for i in range(count):
            self.population.append(infect.Individual(self, infected=infected))

    def update_current_state(self):
        """ This should be done when the time unite is complete - this will update the current state metrics as well as save the details to the history. """
        self.day += 1

        stats = { col:0 for col in self.columns }

        for person in self.population:
            for stat in stats:
                val = getattr(person, stat)
                if val:
                    stats[stat] += 1

        self.history.append(stats)

    @property
    def current_state(self):
        if self.day == -1:
            raise Exception("Cannot call current_state until the first update_current_state has occurred to get the baseline")
            
        return(self.history[-1])

    def current_state_record(self):
        curstate = self.current_state
        return([curstate[col] for col in sorted(self.columns)])

    def fill_days(self):
        """ fill from the current day to the last day with the current record - in effect, saying 'this is the end state but we want data to go to the end of the date range' """
        current_state =self.current_state
        self.history += [current_state for i in range(self.day, self.days)]


    def add_history_from_dataframe(self, df):
        """ Takes the data in the data frame and sets the history from there - this is used for non-agent based engines that calculate infection trajectories independently and need to update the scenario history after the fact. This can be done incrementally."""
        self.history.extend(df.to_dict('records'))
        self.day += len(df)

    @property
    def dataframe(self):
        if self.history is None:
            raise Exception("This model has not been run")
        dataframe = pandas.DataFrame.from_records(self.data, columns=sorted(self.columns))
        return(dataframe)

    @property
    def data(self):
        if self.history is None:
            raise Exception("This model has not been run")
        columns= sorted(self.columns)
        data = []
        for daydata in self.history:
            data.append([daydata[c] if c in daydata else None for c in columns])
        return(data)

    def print_summary(self):
        print(f"Day {self.day}")
        for stat, value in self.current_state.items():
            print(f"{stat}:\t{value}  ("+format(value / self.count, '.1%')+", "+format(value / self.current_state['ever_infected'], '.1%') + ")")       
        print("\n")


    def export(self, filename, **kwargs):
        with open(os.path.expanduser(filename), 'w') as fh:
            writer = csv.writer(fh)
            writer.writerow(sorted(self.columns))
            writer.writerows(self.data)

    def plot(self, filename, **kwargs):
        infect.render.plot(self, filename=filename, **kwargs)

    def display(self, **kwargs):
        infect.render.plot(self, display=True, **kwargs)

