import infect 
import csv 
import os
import pandas

class Engine():
    """ The abstract base class for the model engines """

    def __init__(self, scenario):
        self.scenario = scenario
        self._data = self._dataframe = None

    def run_model(self):
        """run_model should run the model and set either self._data equal to the the raw table dataset or self._dataframe to a pandas dataframe. In either case, the default property will reference the other. """
        raise Exception("build_model must be defined in the subclass.")

