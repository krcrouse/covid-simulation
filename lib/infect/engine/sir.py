import scipy.integrate 
import pandas
import numpy as np
import sys 

from .engine import Engine

'''
Acknolwedgements:
    Resources that contributed to my understanding and development of the SIR model include:
        [The CHIME App: The Model](https://code-for-philly.gitbook.io/chime/what-is-chime/sir-modeling)
        [Florence Debarre, "SIR model of epidemics"](https://ethz.ch/content/dam/ethz/special-interest/usys/ibz/theoreticalbiology/education/learningmaterials/701-1424-00L/sir.pdf)

'''

class SIR(Engine):

    columns = ['never_infected', 'infected', 'recovered']

    def run_model(self):
        
        r0 =  self.scenario.reproduction_number
        gamma = 1.0 / self.scenario.infection.contagious_days
        
        # -- just check
        if self.scenario.beta != self.scenario.reproduction_number * self.scenario.gamma: 
            raise Exception("Beta is wrong")

        
        # set the SIR counts as :
            # N Susceptible
            # N Infected
            # N Recovered  
        current = self.scenario.current_state
        sir_counts = (self.scenario.count - current['infected'], current['infected'], 0)
        timeframe = np.linspace(0, self.scenario.days, self.scenario.days+1) 
        # convert the scenario beta, which the infectivity of a contagious individual, to a population beta, since the SIR calculation is at the population level
        population_beta = self.scenario.infectivity / self.scenario.count
        modeldata = scipy.integrate.odeint(derivative, sir_counts, timeframe, args=(population_beta, self.scenario.gamma))
        
        # update the scenario with the data
        self.scenario.add_history_from_dataframe(pandas.DataFrame(modeldata, columns=self.columns))
        #self._dataframe = pandas.DataFrame(modeldata, columns=self.columns)

def derivative(sir_counts, timeframe, beta, gamma):
    # Equation and understanding greatly helped by
    # https://ethz.ch/content/dam/ethz/special-interest/usys/ibz/theoreticalbiology/education/learningmaterials/701-1424-00L/sir.pdf
    S,I,R = sir_counts
    dSdt = -beta * S * I
    dIdt = beta * S * I - gamma * I
    dRdt = gamma * I
    return(dSdt, dIdt, dRdt)

