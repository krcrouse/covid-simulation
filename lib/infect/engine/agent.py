import random 
import math
import multiprocessing
import pandas
import numpy
import datetime

import infect
from .engine import Engine

class Agent(Engine):


    def __init__(self, *args, update_interval=10, threads=4, **kwargs):
        super().__init__(*args, **kwargs)
        self.threads = threads
        self.update_interval = update_interval

    def run_model(self):

        self._data = []
        for i in range(self.scenario.days):
            self.iterate_day()
            if self.scenario.current_state['infected'] == 0:
                print("\n\n\n**** Infection eradicated on day ", i)
                self.scenario.print_summary()
                # -- fill in extra days 
                self.scenario.fill_days()
                return
            if (i % self.update_interval) == 0:
                self.scenario.print_summary()

    def iterate_day(self):
        if self.threads == 1:
            self._process_new_day()
        else:
            self._process_threads()
        self.scenario.update_current_state()
        self._data.append(self.scenario.current_state_record())


    def _process_threads(self):
        threads = []                
        # divide the population into equal shares 
        count = math.ceil(self.scenario.count / self.threads)
        for subpop in range(self.threads):
            parentpipe, childpipe = multiprocessing.Pipe()
            thread = multiprocessing.Process(target=self._process_thread, args=(childpipe, subpop*count, (subpop+1)*count))
            thread.start()
            threads.append( (thread, parentpipe) )

        #-- now wait for all the responses
        for thread, pipe in threads:
            newly_infected = pipe.recv()
            thread.join()
            for idx in newly_infected:
                self.scenario.population[idx].infect()
        
        #-- now iterate all people to the next day - outside of thread context
        for person in self.scenario.population:
            person.nextday()

    def _process_thread(self, pipe, i_start, i_end):
        if i_end > len(self.scenario.population):
            i_end = len(self.scenario.population)
        infected = []
        for i in range(i_start, i_end):
            person = self.scenario.population[i]
            #
            # Current interaction logic: random from the population with replacement.
            #  - Contact is one-way way, if person 2 is in contact with person 3, we test whether person 2 infects person 3, not vice-versa.
            #  - Therefore, if person i is not contagious, we skip them
            #  - Therefore, the evaluation for Person 3 is indepnedent of the person 2 contact. This needs to be modeled better, but we're just trying to get things working.
            if not person.contagious:
                continue

            for i_contact in range(person.daily_contacts):
                idx = random.randint(0, len(self.scenario.population)-2)
                if idx >= i: # ignore self
                    idx += 1                
                if person.evaluate_contact(self.scenario.population[idx]):
                    infected.append(idx)
        pipe.send(infected)
        pipe.close()

    def _process_new_day(self):

        for i, person in enumerate(self.scenario.population):
        
            person.nextday()
            #
            # Current interaction logic: random from the population with replacement.
            #  - Contact is one-way way, if person 2 is in contact with person 3, we test whether person 2 infects person 3, not vice-versa.
            #  - Therefore, if person i is not contagious, we skip them
            #  - Therefore, the evaluation for Person 3 is indepnedent of the person 2 contact. This needs to be modeled better, but we're just trying to get things working.
            if not person.contagious:
                continue

            for i_contact in range(person.daily_contacts):
                idx = random.randint(0, len(self.scenario.population)-2)
                if idx >= i: # ignore self
                    idx += 1
                person.contact(self.scenario.population[idx])

