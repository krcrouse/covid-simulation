import sys, re, os
import traceback, warnings, pdb
from pprint import pprint
import time, datetime
import random 

""" This example script sets scenario infection characteristics based on 'doubling time' and runs both the mathemtical SIR model and the general agent based model. """


sys.path.append('lib')
import infect

import argparse
parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument('--debug', '-d', action='store_true', help='Print extra messages but still save/commit.')
args = parser.parse_args()


def main():
    try:

        for doubling_time in (4, 10, 15):

            scenario = infect.Scenario(infect.infection.SimpleCOVID())
            scenario.add_population(10000)
            scenario.add_population(5, infected=True)
            scenario.set_doubling_rate(doubling_time)
            scenario.update_current_state() 

            r0 = scenario.reproduction_number
            print("Rho-0", r0)
            print("Beta", scenario.infection.transmissability)
            print("Doubling rate", scenario.doubling_rate())

            sir_context = scenario.copy_context()
            sir_engine = infect.engine.SIR(sir_context)
            sir_engine.run_model()
            sir_context.plot(f'output/sir/sir.'+format(r0, '.3f')+'.png')
            sir_context.export(f'output/sir/sir.'+format(r0, '.3f')+'.csv')
        
            agent_context = scenario.copy_context()
            agent_engine = infect.engine.Agent(agent_context)
            agent_engine.run_model()
            agent_context.plot(f'output/simulated_sir/sir'+format(r0, '.3f')+'.png')
            agent_context.export(f'output/simulated_sir/sir.'+format(r0, '.3f')+'.csv')

    except Exception as e:
        print("\n\n**** ERRROR ******")
        type, value, tb = sys.exc_info()
        traceback.print_exc()
        if self.debug:
            pdb.post_mortem(tb)
        raise

main()
